from django.urls import path, include
from . import views
app_name = 'companies'

urlpatterns = [
    path('api/committee/', views.CompanyCommittee.as_view(), name='committee'),
    path('api/committee/<int:pk>/',
         views.CompanyCommitteeView.as_view(), name='committee'),
    path('api/company/', views.CompanyCreateRetrieveView.as_view(), name='company'),
    path('api/branch/', views.BranchCreateRetrieveView.as_view(), name='branch'),
    path('api/hr/', views.HRCreateRetrieveView.as_view(), name='hr'),
    path('api/forcelearning/', views.ForceLearningView.as_view(),
         name='force_learning'),
]
