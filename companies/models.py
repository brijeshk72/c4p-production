from django.db import models

from users.models import User
from core.models import StatusMixin, AddressMixin, ProfileMixin, MobileMixin
from learning.models import Language
# Create your models here.


class Company(StatusMixin, AddressMixin, MobileMixin):
    name = models.CharField(max_length=264, null=True, blank=True)

    def __str__(self):
        return self.name


class Branch(StatusMixin, AddressMixin, ProfileMixin):
    name = models.CharField(max_length=264, null=True, blank=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='branches')

    def __str__(self):
        return "{} in {}".format(self.name, self.company.name)


class UserWorkProfile(StatusMixin, Language):

    USER_CHOICE = (
        (1, 'Director'),
        (2, 'HR'),
        (3, 'Employee'),
        (4, 'Internal Committee')
    )

    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='company_work_profile')
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='user_profile')
    user_type = models.IntegerField(choices=USER_CHOICE, default=1)
    branches = models.ManyToManyField(
        Branch, related_name='branch_user_profile', blank=True)

    def __str__(self):
        return "{} in {}".format(self.user.username, self.company)


class InternalCommitee(StatusMixin, MobileMixin):
    GENDER_CHOICE = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    name = models.CharField(max_length=200, null=True, blank=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='company_committee')
    email = models.EmailField(max_length=250, null=True, blank=True)
    gender = models.CharField(choices=GENDER_CHOICE,
                              null=True, blank=True, max_length=1)
    userprofile = models.OneToOneField(
        UserWorkProfile, on_delete=models.CASCADE, related_name='user_internal_committee', null=True, blank=True)
    isheadOfficer = models.BooleanField(default=False)

    class Meta:
        unique_together = ('company', 'mobile', 'email',)

    def __str__(self):
        return self.name


class CompanyPolicy(StatusMixin):
    COMPANY_POLICY = (
        (1, "GENDER NEUTRAL POLICY"),
        (2, "FEMALE ORIENTED POLICY"),
    )
    company = models.OneToOneField(
        Company, related_name='company_policy', on_delete=models.CASCADE)
    valid_upto = models.DateTimeField(null=True, blank=True)
    type_of_policy = models.IntegerField(choices=COMPANY_POLICY, default=1)
    policy = models.FileField(null=True, blank=True)


class ForceLearning(StatusMixin):
    force_learning_quiz = models.BooleanField(default=False)
    force_learning_model = models.BooleanField(default=False)
    force_learning_both = models.BooleanField(default=False)
    force_time_quiz = models.PositiveIntegerField(default=0)
    force_time_model = models.PositiveIntegerField(default=0)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='force_learning')

    def __str__(self):
        return "Quiz"+str(self.force_learning_quiz)+"model"+str(self.force_learning_model)
