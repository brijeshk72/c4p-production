from rest_framework.views import APIView
import json
from pprint import pprint
from .serializers import *
from .models import *
from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from django.db import IntegrityError
from .mixins import DirectorDispatchMixin,DispatchLoginMixin
from users.models import User
from rest_framework.authtoken.models import Token
from license.models import LicenseModel

class CompanyCreateRetrieveView(DispatchLoginMixin):
    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body.decode('utf-8'))
        pprint(data_json)
        try:
            serialized_company = CompanySerializer(data=data_json)
            if serialized_company.is_valid():
                company = serialized_company.save()
                # Create Force Learning object when new company create
                ForceLearning.objects.create(company_id=company.id)
                #Create User Work Profile
                work_profile = UserWorkProfile.objects.create(
                    user = self.valid_user,
                    user_type = 1,
                    company = company
                )
                serialized_profile = UserWorkProfileSerializer(work_profile)
                return Response({
                    'company':serialized_company.data,
                    'profile':serialized_profile.data
                })
            else:
                return Response(
                    serialized_company.errors
                )
        except Exception as e:
            return Response({
                "status":False,
                "message":e
            })


    def get(self, request, *args, **kwargs):
        try:
            work_profile = UserWorkProfile.objects.get(user=self.valid_user)
            if work_profile.user_type != 1:
                return Response({
                    "status":False,
                    "message":"User Is Not The Director"
                })
            company = work_profile.company
            company_serializer = CompanySerializer(company)
            return Response(company_serializer.data)
        except UserWorkProfile.DoesNotExist:
            return Response({
                "status":False,
                "message":"UserWorkProfile Does Not Exist"
            })


class BranchCreateRetrieveView(DirectorDispatchMixin):

    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body.decode('utf-8'))
        print(data_json)
        data_json['company'] = self.work_profile.company.pk
        print(data_json)
        try:
            serialized_branch = BranchSerializer(data=data_json)

            if serialized_branch.is_valid():
                branch = serialized_branch.save()
                return Response(serialized_branch.data)
            else:
                return Response(serialized_branch.errors)
        except Exception as e:
            return Response({
                "status":False,
                "message":e,
            })

    def get(self, request, *args, **kwargs):
        try:
            company = self.work_profile.company
            branches = Branch.objects.filter(company_id = company.id)
            branch_list = []
            for branch in branches:
                branch_list.append({
                    'id':branch.id,
                    'name':branch.name,
                    'company':branch.company.name,
                    'email':branch.email,
                    'mobile':branch.mobile,
                    'country':branch.country,
                    'state':branch.state,
                    'city':branch.city,
                    'zip_code':branch.zip_code
                    })
            return Response(branch_list)
        except UserWorkProfile.DoesNotExist:
            return Response('UserWorkProfile Does not exit')

        # try:
        #     company = self.work_profile.company
        #     branches = Branch.objects.filter(company=company)
        #     serialized_brances = BranchSerializer(branches,many=True)
        #     return Response(serialized_brances.data)
        # except UserWorkProfile.DoesNotExist:
        #     return Response({
        #         "status": False,
        #         "message": "UserWorkProfile Does Not Exist"
        #     })


class HRCreateRetrieveView(DirectorDispatchMixin):

    def post(self, request, *args, **kwargs):
        company = self.work_profile.company

        data = json.loads(request.body.decode('utf-8'))
        hr = User.objects.create(
            username = data.get('username'),
            email = data.get('email'),
            mobile = data.get('mobile'),
        )
        hr.set_password(data.get('password'))
        hr.save()
        print("HR HERE",hr)
        token, created = Token.objects.get_or_create(user=hr)
        work_profile = UserWorkProfile.objects.create(
            user = hr,
            user_type = 2,
            company = company,
        )
        for branch in data.get("branches"):
            work_profile.branches.add(branch)

        serialzed_work_profile = UserWorkProfileSerializer(work_profile)
        return Response(serialzed_work_profile.data)



    def get(self, request, *args, **kwargs):
        company = self.work_profile.company
        hrs = UserWorkProfile.objects.filter(user_type=2, company_id = company.id)
        hr_list = []
        for hr in hrs:
            hr_list.append({
                'id':hr.id,
                'username':hr.user.username,
                'email':hr.user.email,
                'mobile':hr.user.mobile,
                'company':hr.company.name,
                'branch':hr.branches.name
                })
        # print(hr_list)
        return Response(hr_list)

        # company = self.work_profile.company
        # all_hr = UserWorkProfile.objects.filter(company=company,user_type=2)
        # serialized_profiles = UserWorkProfileSerializer(all_hr,many=True)
        # print(serialized_profiles)
        # return Response(serialized_profiles.data)


class CompanyCommittee(DirectorDispatchMixin):

    def post(self,request,*args,**kwargs):
        company = self.work_profile.company
        data_json = json.loads(request.body.decode('utf-8'))
        print(data_json)
        try:
            print(data_json)
            # usernameCompanyCommittee = data_json.get('email')
            # password = data_json.get('password')
            user = User.objects.create(
                first_name = data_json.get('first_name'),
                last_name = data_json.get('last_name'),
                username = data_json.get('email'),
                email = data_json.get('email'),
                mobile = data_json.get('mobile'),

            )
            print(data_json)
            print(user)
            # user.set_password(password)
            # user.save()
            token, created = Token.objects.get_or_create(user=user)
            user_profile = UserWorkProfile.objects.create(
                company = company,
                user=user,
                user_type=4,
            )
            print(user)
            internal_committee = InternalCommitee.objects.create(
                name = data_json.get('first_name')+ " "+data_json.get('last_name'),
                company=company,
                userprofile=user_profile,
                email=data_json.get('email'),
                mobile = data_json.get('mobile'),
                gender = data_json.get('gender'),
                isheadOfficer = data_json.get('ishead')
            )
            serialized_committe = InternalCommitteeSerializer(internal_committee)
            print(serialized_committe)
            return Response(
                {   
                    "committee":serialized_committe.data,
                    "token":token.key
                }
            )
        except:
            return Response({
                "status":False,
                "message":"Integrity Error",
                "user_info":"username must be Unique"
            })

    def get(self,request,*args,**kwargs):
        company = self.work_profile.company

        committee = InternalCommitee.objects.filter(company_id=company.id)

        committee_list = []
        for ic in committee:
            committee_list.append({
                'id':ic.id,
                'name':ic.name,
                'company':ic.company.name,
                'userprofile':ic.userprofile.user.username,
                'email':ic.email,
                'mobile':ic.mobile,
                'gender':ic.gender,
                'isheadOfficer':ic.isheadOfficer
                })

        return Response(committee_list)


# Retrive Single Internal Committeee Data
class CompanyCommitteeView(DirectorDispatchMixin):

    def get(self, request, pk):
        try:
            print(pk)
            committee = InternalCommitee.objects.get(pk=pk)
            committee_info = {
                'id':committee.id,
                'name':committee.name,
                'company':committee.company.name,
                'userprofile':committee.userprofile.user.username,
                'email':committee.email,
                'mobile':committee.mobile,
                'gender':committee.gender,
                'isheadOfficer':committee.isheadOfficer
            }
            return Response(committee_info)
        except InternalCommitee.DoesNotExist:
            return Response({
                "message":"Data Not Found"
            })





# class CompanyPolicy(DispatchLoginMixin):
#
#     def get(self,request,*args,**kwargs):
#         company = Company.objects.get(director=self.valid_user)
#         policy = CompanyPolicy.objects.get(company=company)
#
#         return Response({
#             'id':policy.pk,
#             'policy':policy.policy
#         })
#
#     def post(self,request,*args,**kwargs):
#         company = Company.objects.get(director=self.valid_user)
#         data = request.POST
#
#         policy = CompanyPolicy.objects.create(
#             company=company,
#             policy=data.get('policy'),
#             valid_upto="AS"
#
#         )
#         pass


class ForceLearningView(DispatchLoginMixin, DirectorDispatchMixin):
    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body.decode('utf-8'))
        print(data_json)
        print(self.work_profile.company)
        # try:

            # force = ForceLearning.objects.get(company_id=self.work_profile.company.id)
        
            # try:
            #     if data_json['force_learning_both'] == True:
            #         force.force_learning_quiz = True
            #         force.force_learning_model = True
            #         force.force_learning_both = True
            #         force.save()
            #     else:
            #         force.force_learning_quiz = False
            #         force.force_learning_model = False
            #         force.force_learning_both = False
            #         print("False Block")
            #         print("data save")
            #         force.save()
                
            #     return Response("Forced Learning For Quiz And Model")
            # except:
        try:
            force = ForceLearning.objects.get(company_id=self.work_profile.company.id)
            if data_json['force_learning_quiz'] == True:
                
                try:
                    force.force_learning_quiz = True
                    force.force_time_quiz = data_json['force_time_quiz']
                    force.save()
                    return Response("Quiz Time Updated")
                except:
                    return Response("Please Fill The Quiz Time")
            else:
                force.force_learning_quiz = False
                force.save()
                return Response("Forced Learning Off for Quiz")

        except:
            try:
                if data_json['force_learning_model']:
                    try:
                        force.force_learning_model = True
                        force.force_time_model = data_json['force_time_model']
                        force.save()
                        return Response("Model Time Updated")
                    except:
                        return Response("Please Fill The Model Time")
                else:
                    force.force_learning_model = False
                    force.save()
                    return Response("Forced Learning Off For Model")
            except:
                return Response("Some Data Are Missing for Model")

        # except ForceLearning.DoesNotExist:
        #     ForceLearning.objects.create(
        #         company=self.work_profile.company
        #     )
        #     return Response("Company ForceLearning Created")


    def get(self, request, *args, **kwargs):
        force = ForceLearning.objects.get(company_id=self.work_profile.company.id)
        # force_list = []
        # for force in forces:
            # force_list.append({
            #     "id":force.id,
            #     "force_learning_quiz":force.force_learning_quiz,
            #     "force_learning_model":force.force_learning_model,
            #     # "force_learning_both":force.force_learning_both,
            #     "force_time_quiz":force.force_time_quiz,
            #     "force_time_model":force.force_time_model,
            #     "company":force.company.name
            # })
        

        force_list = {
                "id":force.id,
                "force_learning_quiz":force.force_learning_quiz,
                "force_learning_model":force.force_learning_model,
                "force_time_quiz":force.force_time_quiz,
                "force_time_model":force.force_time_model,
                "company":force.company.name
            }

        return Response(force_list)