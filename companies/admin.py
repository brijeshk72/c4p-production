from django.contrib import admin
from easy_select2 import select2_modelform
from .models import *

admin.site.site_header = 'Center 4 Posh'
admin.site.site_title = 'Center 4 Posh'
admin.site.index_title = 'site administration'


#
# # Register your models here.
SelectForm = select2_modelform(Company, attrs={'width': '500px'})
#
@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    model = Company
    list_display = ['id', 'name', 'mobile']
    list_filter = ['name', ]
    search_fields = ('name', )
#
    form = SelectForm


SelectForm = select2_modelform(UserWorkProfile, attrs={'width': '500px'})
#
@admin.register(UserWorkProfile)
class UserWorkProfileAdmin(admin.ModelAdmin):
    model = UserWorkProfile
    list_display = ['user', 'company', 'user_type']
    list_filter = ['company__name', ]
    search_fields = ('user__username', 'company__name')
#
    form = SelectForm


#
SelectForm = select2_modelform(Branch, attrs={'width': '500px'})
#
@admin.register(Branch)
class BranchAdmin(admin.ModelAdmin):
    model = Branch
    list_display = ['id', 'name', 'company', 'zip_code',
                    'company', 'active', 'first_name', 'email', 'mobile']
    list_filter = ['company__name']
    search_fields = ('name', "company__name")

    form = SelectForm


#
#
SelectForm = select2_modelform(InternalCommitee, attrs={'width': '500px'})


@admin.register(InternalCommitee)
class InternalCommiteeAdmin(admin.ModelAdmin):
    model = InternalCommitee
    list_display = ['id', 'userprofile', 'gender',
                    'mobile', 'email', 'company', 'isheadOfficer']
    list_filter = ['company__name', ]
    search_fields = ('company__name', 'mobile')

    form = SelectForm


@admin.register(CompanyPolicy)
class CompanyPolicyAdmin(admin.ModelAdmin):
    model = CompanyPolicy
    list_display = ['company', 'valid_upto', 'type_of_policy']
    list_filter = ['company__name', 'type_of_policy']
    search_fields = ('company__name', 'type_of_policy')

    form = SelectForm


class ForceLearningAdmin(admin.ModelAdmin):
    list_display = ('id', 'force_learning_quiz',
                    'force_learning_model', 'force_time_quiz', 'force_time_model', 'company')


admin.site.register(ForceLearning, ForceLearningAdmin)
