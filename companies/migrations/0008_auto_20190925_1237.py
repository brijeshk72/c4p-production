# Generated by Django 2.2 on 2019-09-25 12:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0007_auto_20190925_0900'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='branch',
            name='email',
        ),
        migrations.RemoveField(
            model_name='branch',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='branch',
            name='gender',
        ),
        migrations.RemoveField(
            model_name='branch',
            name='last_name',
        ),
        migrations.RemoveField(
            model_name='branch',
            name='mobile',
        ),
    ]
