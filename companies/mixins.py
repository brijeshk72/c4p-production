from rest_framework import serializers
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from django.http import JsonResponse
from companies.models import UserWorkProfile
from license.models import LicenseModel


class RepresentationMixin(serializers.ModelSerializer):
    def to_representation(self, instance):
        result = super().to_representation(instance)
        for key in result:
            if result[key] == None:
                result[key] = 'N/A'
        return result


class DirectorDispatchMixin(APIView):

    work_profile = None

    def dispatch(self, request, *args, **kwargs):
        try:
            user, token = TokenAuthentication().authenticate(request)
            self.work_profile = UserWorkProfile.objects.get(user=user)
            if self.work_profile.user_type != 1:
                return JsonResponse({
                    "status": False,
                    "message": "User Is Not The Director"
                })
        except AuthenticationFailed as e:
            return JsonResponse({"message": "Invalid Auth Token"}, status=status.HTTP_401_UNAUTHORIZED)
        return super().dispatch(request, *args, **kwargs)


class DispatchLoginMixin(APIView):

    valid_user = None

    def dispatch(self, request, *args, **kwargs):
        try:
            user, token = TokenAuthentication().authenticate(request)
            self.valid_user = user
        except AuthenticationFailed as e:
            return JsonResponse({"message": "Invalid Auth Token"}, status=status.HTTP_401_UNAUTHORIZED)
        return super().dispatch(request, *args, **kwargs)


class DispatchLicenseMixin(APIView):
    valid_license = None

    def dispatch(self, request, *args, **kwargs):
        try:
            user, token = TokenAuthentication().authenticate(request)
            user_profile = UserWorkProfile.objects.get(user=user)
            self.valid_license = LicenseModel.objects.get(
                user_profile=user_profile)
        except AuthenticationFailed as e:
            return JsonResponse({"message": "Invalid Auth Token"}, status=status.HTTP_401_UNAUTHORIZED)
        return super().dispatch(request, *args, **kwargs)
