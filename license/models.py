from django.db import models
from companies.models import Company, UserWorkProfile, Branch
import uuid
from users.models import User

# Create your models here


class CompanyBuffer(models.Model):
    total_licenses = models.IntegerField(default=0)
    company = models.OneToOneField(
        Company, on_delete=models.CASCADE, related_name='company_buffer')

    def __str__(self):
        return str(self.total_licenses)


class LicenseModel(models.Model):
    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, max_length=12)
    license = models.CharField(
        max_length=200, null=True, blank=True, editable=False)
    added_by = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True)
    user_profile = models.OneToOneField(
        UserWorkProfile, on_delete=models.SET_NULL, null=True, blank=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, null=True, blank=True)
    branch = models.ForeignKey(
        Branch, on_delete=models.SET_NULL, null=True, blank=True)

    # price = models.DecimalField(decimal_places=2, default=0, max_digits=10)

    active_license = models.BooleanField(default=False)

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female')
    )
    gender = models.CharField(max_length=1, null=True,
                              blank=True, choices=GENDER_CHOICES)

    def get_gender(self):
        return GENDER_CHOICES[self.gender]

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        li = "LCN" + str(self.uuid)
        self.license = li
        super().save()

    def __str__(self):
        return str(self.license)
