from django.urls import path
from .views import *
app_name = 'license'

urlpatterns = [
    path('api/license/', EmployeeCreateRetrieveView.as_view(),
         name='employee_license'),

    path('api/license/excel/', EmployeeLicenseExcelView.as_view(),
         name='employee_license_excel_view'),

    path('api/license/forcelearning/',
         EmployeeForceLearningView.as_view(), name='employeeforcelearning'),
    path('api/license/login/', LicenseViews.as_view(), name='licenseviews'),

    #     path('api/license/alert/', LicenseAlertMessage.as_view(),
    #          name='LicenseAlertMessage'),
    path('api/license/csv/', LicenseCSV.as_view(), name='export_users_csv'),
]
