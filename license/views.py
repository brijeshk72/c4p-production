from rest_framework.authentication import TokenAuthentication
import xlwt
import csv
from django.http import HttpResponse
import json
from companies.mixins import DispatchLoginMixin, DirectorDispatchMixin, DispatchLicenseMixin
from users.models import User
from companies.models import UserWorkProfile, Branch, ForceLearning
from rest_framework.response import Response
from .models import LicenseModel, CompanyBuffer
from .serializers import LicenseSerializer
from rest_framework.authtoken.models import Token
from django.core.mail import send_mail
from rest_framework.views import APIView
# from rest_framework.views import APIView
# pandas use for mass License issue
from pandas import *
import pandas as pd


class EmployeeCreateRetrieveView(DirectorDispatchMixin):

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        company = self.work_profile.company
        try:
            buffer = CompanyBuffer.objects.get(company=company)
            licenses_created = LicenseModel.objects.filter(
                company=company).count()
            if licenses_created >= buffer.total_licenses:
                return Response({
                    "status": False,
                    "message": "No More Licenses Available"
                })
        except CompanyBuffer.DoesNotExist:
            return Response({
                "status": False,
                "message": "License Number Not Defined"
            })
        user = User.objects.filter(email=data.get('email')).first()
        if user:
            return Response({
                "status": False,
                "message": "Email Already Exist"
            })
        print(data.get('email'))
        print(data.get('mobile'))
        username=data.get('username')
        branch = Branch.objects.get(pk=int(data.get('branch')))
        user = User.objects.create(
            first_name = username,
            username=data.get('email'),
            email=data.get('email'),
            mobile=data.get('mobile'),
        )

        token, created = Token.objects.get_or_create(user=user)
        emp_user_profile = UserWorkProfile.objects.create(
            user=user,
            company=company,
            user_type=3,
        )
        emp_user_profile.save()
        emp_user_profile.branches.add(branch)
        license = LicenseModel.objects.create(
            user_profile=emp_user_profile,
            company=company,
            branch=branch,
        )
        print(license)
        serialized_license = LicenseSerializer(license)
        buffer = CompanyBuffer.objects.get(company=company)
        buffer.total_licenses -= 1
        buffer.save()
        mymail = send_mail('C4P issued a License', license.license, 'centre4posh@gmail.com', [
            data.get('email')], fail_silently=False)

        print(mymail)

        return Response(
            {
                "license": serialized_license.data,
                "token": token.key
            }
        )

    def get(self, request, *args, **kwargs):
        company = self.work_profile.company
        print(company, '==============')

        licenses = LicenseModel.objects.filter(company=company)
        license_list = []
        for lic in licenses:
            license_list.append({
                "UUID": lic.uuid,
                "license": lic.license,
                "user_profile": lic.user_profile.user_type,
                "name": lic.user_profile.user.first_name + " " + lic.user_profile.user.last_name,
                "username": lic.user_profile.user.username,
                "email": lic.user_profile.user.email,
                "company": lic.company.name,
                "branch": lic.branch.name,
                # "added_by": lic.added_by,
                'gender': lic.gender,
                "active_license": lic.active_license
            })

        total_license = CompanyBuffer.objects.get(company=company)

        license_accessed = LicenseModel.objects.filter(
            active_license=True, company=company).count()

        return Response({
            "total_license": total_license.total_licenses,
            "license_issued": licenses.count(),
            "license_left": total_license.total_licenses-licenses.count(),
            "license_accessed": license_accessed,
            "license_unaccessed": licenses.count() - license_accessed,
            "license_male": LicenseModel.objects.filter(gender='M', active_license=True, company=company).count(),
            "license_female": LicenseModel.objects.filter(gender='F', active_license=True, company=company).count(),
            "license_list": license_list
        })


class LicenseViews(APIView):
    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body.decode('utf-8'))
        try:
            license = data_json['license']
            lic = LicenseModel.objects.get(license=license)
            if lic:
                usr = User.objects.get(id=lic.user_profile.user.id)
                token, _ = Token.objects.get_or_create(user=usr)

                if lic.active_license == False:
                    lic.active_license = True
                    lic.save()

                return Response({'message': lic.license, 'token': token.key})
            else:
                return Response({'message': 'No licence exist'})
        except LicenseModel.DoesNotExist:
            return Response({"message": "Please Enter the valid License Number"})


class EmployeeForceLearningView(DispatchLicenseMixin):
    def get(self, request, *args, **kwargs):

        try:
            lnc = LicenseModel.objects.get(license=self.valid_license)
            force = ForceLearning.objects.get(company_id=lnc.company.id)
            forceList = {
                "id": force.id,
                "force_learning_quiz": force.force_learning_quiz,
                "force_learning_model": force.force_learning_model,
                "force_time_quiz": force.force_time_quiz,
                "force_time_model": force.force_time_model,
                "company": force.company.name
            }
            return Response(forceList)
        except ForceLearning.DoesNotExist:
            return Response({"message": "Data Not Found"})



class EmployeeLicenseExcelView(DirectorDispatchMixin):

    def post(self, request, *args, **kwargs):
        # data = json.loads(request.body.decode('utf-8'))
        # print(data)
        company = self.work_profile.company
        try:
            buffer = CompanyBuffer.objects.get(company=company)
            licenses_created = LicenseModel.objects.filter(
                company=company).count()
            if licenses_created >= buffer.total_licenses:
                return Response({
                    "status": False,
                    "message": "No More Licenses Available"
                })
        except CompanyBuffer.DoesNotExist:
            return Response({
                "status": False,
                "message": "License Number Not Defined"
            })

        already_register_user = []
        new_user = []
        # =============================================
        if request.method == 'POST':
            myf = request.FILES['myfile']
            try:
                df = pd.read_excel(myf)
            except:
                message = "File '"+ str(myf)+ "' Not Supported, Please Enter Excel File Only"
                return Response({"message":message})
                # df = pd.read_csv(myf)

            df = df.replace({pd.np.nan: None})

            print(df)

            names = df['name'].values.tolist()
            
            emails = df['email'].values.tolist()

            branches = df['branch'].values.tolist()

            users = []
            for n, s, b in zip(names, emails, branches):
                newUser = {
                'name':n,
                'email':s,
                'branch':b
                }
                users.append(newUser)

            for i in users:
                # print(i)
                for k, v in i.items():
                    if "name"==k:
                        name = v
                        
                    if "email"==k:
                        email = v
    
                    if "branch" == k:
                        branch = v

        # ===========================================
                user = User.objects.filter(email=email).first()
                if user:
                    already_register_user.append(email)
                    # return Response({
                    #     "status": False,
                    #     "message": "Email Already Exist"
                    # })

                else:
                    new_user.append(email)
                    print(email)
                    print(branch)
                    print(name)
                    username=name
                    branch = Branch.objects.get(pk=int(branch))
                    user = User.objects.create(
                        first_name = username,
                        username=email,
                        email=email
                    )

                    token, created = Token.objects.get_or_create(user=user)
                    emp_user_profile = UserWorkProfile.objects.create(
                        user=user,
                        company=company,
                        user_type=3,
                    )
                    emp_user_profile.save()
                    emp_user_profile.branches.add(branch)
                    license = LicenseModel.objects.create(
                        user_profile=emp_user_profile,
                        company=company,
                        branch=branch,
                    )
                    print(license)
                    serialized_license = LicenseSerializer(license)
                    buffer = CompanyBuffer.objects.get(company=company)
                    buffer.total_licenses -= 1
                    buffer.save()
                    mymail = send_mail('C4P issued a License', license.license, 'centre4posh@gmail.com', [email], fail_silently=False)

                    print(mymail)
            
            return Response(
                {   
                    "license": serialized_license.data,
                    "token": token.key,
                    "new_user":new_user,
                    "already_register_user":already_register_user
                }
            )



# license Messege alert
# from django.contrib import messages
# class LicenseAlertMessage(DirectorDispatchMixin):
#     def get(self, request, *args, **kwargs):
#         company = self.work_profile.company
#         totalLicense =CompanyBuffer.objects.get(company= company)

#         issuedLicense = LicenseModel.objects.filter(company=company).count()

#         perLicense = float(issuedLicense)*(100.0/float(totalLicense.total_licenses))
#         myvar = str(perLicense)+"%/ License Used"
#         if perLicense > 90.0:
#             message = messages.info(request, myvar) 

#         return Response({
#             "message":message
#         })





























class LicenseCSV(DispatchLoginMixin):
    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachement; filename="license.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('LicenseModel')

        row_num = 0
        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['license', 'active_license']

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

        font_style = xlwt.XFStyle()

        rows = LicenseModel.objects.all().values_list('license', 'active_license')

        for row in rows:
            row_num += 1
            for col_num in range(len(row)):
                ws.write(row_num, col_num, row[col_num], font_style)

        wb.save(response)
        return response

        # old Code
        # response = HttpResponse(content_type='text/csv')
        # response['Content-Disposition'] = 'attachement ;filename="license.csv"'

        # write = csv.writer(response)
        # write.writerow(['license'])

        # licenses = LicenseModel.objects.all().values_list('license')

        # for license in licenses:
        #     write.writerow(license)

        # return response


# from import_export import resources

# class LicenseModelResource(resources.ModelResource):

#     class Meta:
#         model = LicenseModel
