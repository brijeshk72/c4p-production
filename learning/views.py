from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import *
from .serializers import *
from django.http import Http404
import json
from companies.mixins import DispatchLoginMixin
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from companies.models import UserWorkProfile
import random


class LanguageView(DispatchLoginMixin):

    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body.decode("utf-8"))
        print(data_json)
        user = UserWorkProfile.objects.get(user=self.valid_user)
        if data_json['lang']:
            user.language = data_json['lang']
            user.save()
            print(user.language)
        return Response('Language Change to {}'.format(user.language) , status=status.HTTP_201_CREATED)

# Chapter Api View
class ChapterView(DispatchLoginMixin):
    """
    Retrieve and Create Chapter
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))

        title = data_json['title']
        active = data_json['active']

        Chapter.objects.create(title=title, active=active)

        return Response('Chapter created', status=status.HTTP_201_CREATED)

    def get(self,request,*args,**kwargs):
        user_lang = UserWorkProfile.objects.get(user=self.valid_user)
        print(user_lang)
        chapter = Chapter.objects.filter(language=user_lang.language)
        serializer = ChapterSerializer(chapter, many=True)
        return Response(serializer.data)


class Chapter_View(DispatchLoginMixin):
    """
    Retrieve, update or delete instance.
    """
    def get_object(self, pk):
        try:
            return Chapter.objects.get(pk=pk)
        except Chapter.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        chapter = self.get_object(pk)
        serializer = ChapterSerializer(chapter)
        return Response(serializer.data)

    def put(self, request,pk):
        chapter = self.get_object(pk)
        serializer = ChapterSerializer(chapter, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        chapter = self.get_object(pk)
        chapter.delete()
        return Response('Chapter deleted', status=status.HTTP_204_NO_CONTENT)



# Section Api View
class SectionView(DispatchLoginMixin):
    """
    Retrieve and Create Section
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))

        title = data_json['title']
        chapter = Chapter.objects.get(id=data_json['chapter_id'])
        description = data_json['description']
        active = data_json['active']

        Section.objects.create(title=title, chapter=chapter, description=description, active=active)
        return Response('Section created', status=status.HTTP_201_CREATED)

    def get(self,request,*args,**kwargs):
        section = Section.objects.order_by('?')[:5]
        serializer = SectionSerializer(section, many=True)
        return Response(serializer.data)


class Section_View(DispatchLoginMixin):
   """
   Retrieve, update or delete a section instance.
   """
   def get(self, request, pk):
       try:
           section_id= Section.objects.get(pk=pk)
           id = section_id.id
       except Section.DoesNotExist:
           return Response('Chapter containing Section Not found')
       section = Section.objects.get(pk=id)
       data = section.description
       return Response(data)



class Section_Views(DispatchLoginMixin):
    """
    Retrieve, update or delete a section instance.
    """
    # def get_object(self, pk):
    #     try:
    #         return Section.objects.get(pk=pk)
    #     except Section.DoesNotExist:
    #         raise Http404


    def get(self, request, pk):
        try:
            chapter_id= Chapter.objects.get(pk=pk)
        except Chapter.DoesNotExist:
            return Response('Chapter containing Section Not found')    
        section = Section.objects.filter(chapter_id=chapter_id)
        data = []
        for s in section:
            data.append({
                "id": s.id,
                "title": s.title,
                "description": s.description,
            })
        return Response(data)


    def put(self, request,pk):
        section = self.get_object(pk)
        serializer = SectionSerializer(section, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)  

    def delete(self, request, pk):
        section = self.get_object(pk)
        section.delete()
        return Response('Section deleted', status=status.HTTP_204_NO_CONTENT)


# Quiz Api view
class QuizView(DispatchLoginMixin):
    """
    Retrieve and Create Question
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))

        try:
            quiz = Quiz.objects.get(id=data_json['quiz_id'])

            if quiz.forced_learning == True:
                quiz.forced_learning = False
            elif quiz.forced_learning == False:
                quiz.forced_learning = True

            quiz.save()
            return Response('Quiz Updated For Forced Learning', status=status.HTTP_201_CREATED)

        except:
            try:
                title = data_json['title']
                chapter = Chapter.objects.get(id=data_json['chapter_id'])
                section = Section.objects.get(id=data_json['section_id'])
                active = data_json['active']
                forced_learning = data_json['forced_learning']
                totalMarks = data_json['totalMarks']

                Quiz.objects.create(title=title, chapter=chapter, section=section, active=active, forced_learning=forced_learning, totalMarks=totalMarks)
                return Response('Quiz created', status=status.HTTP_201_CREATED)
            except:
                return Response('Some data are missing, please Fill it again')


    def get(self,request,*args,**kwargs):
        
        quiz = Quiz.objects.all()
        serializer = QuizSerializer(quiz, many=True)
        return Response(serializer.data)

class Quiz_View(DispatchLoginMixin):
    """
    Retrieve, update or delete a quiz instance.
    """
    # def get_object(self, pk):
    #     try:
    #         return Quiz.objects.get(pk=pk)
    #     except Quiz.DoesNotExist:
    #         raise Http404


    def get(self, request, pk):
        try:
            section_id = Section.objects.get(pk=pk)
        except Section.DoesNotExist:
            return Response('Quiz Not found')

        quiz = Quiz.objects.filter(section_id=section_id.id, active=True)
        quiz_list = []
        for q in quiz:
            quiz_list.append({
                'quiz_id':q.id,
                'quiz_title':q.title,
                })

        data = {
        'section_id':section_id.id,
        'section_title':section_id.title,
        'quiz_list':quiz_list
        }

        return Response(data)




    # def get(self, request, pk):
    #     try:
    #         quiz_id = Quiz.objects.get(pk=pk)
    #     except Quiz.DoesNotExist:
    #         return Response('Quiz Not found')

    #     question = Question.objects.filter(quiz_id=quiz_id.id)
    #     question_list = []
    #     for q in question:
    #         question_list.append({
    #             'question_id':q.id,
    #             'question_title':q.title,
    #             'question_active':q.active
    #             })

    #     data = {
    #     'quiz_id':quiz_id.id,
    #     'quiz_title':quiz_id.title,
    #     'quiz_active':quiz_id.active,
    #     'question_list':question_list
    #     }

    #     print(data)
    #     return Response(data)



    # def get(self, request, pk):
    #     quiz = self.get_object(pk)
    #     serializer = QuizSerializer(quiz)
    #     return Response(serializer.data)

    def put(self, request,pk):
        quiz = self.get_object(pk)
        serializer = QuizSerializer(quiz, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        quiz = self.get_object(pk)
        quiz.delete()
        return Response('1 quiz deleted', status=status.HTTP_204_NO_CONTENT)


# Question Api View
class QuestionView(DispatchLoginMixin):
    """
    Retrieve and Create Question
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))

        title = data_json['title']
        quiz = Quiz.objects.get(id=data_json['quiz_id'])
        active = data_json['active']
        score = data_json['score']
        # question_time = data_json['question_time']

        Question.objects.create(title=title, quiz=quiz, score=score, active=active)

        return Response('1 quiz created', status=status.HTTP_201_CREATED)

    def get(self,request,*args,**kwargs):
        
        # question = Question.objects.all()
        # print(question)
        # serializer = QuestionSerializer(question, many=True)
        # return Response(serializer.data)

        questions = Question.objects.all()
        question_list = []
        for q in questions:
            option_list = []
            options = Option.objects.filter(question_id=q.id)
            for option in options:
                option_list.append({
                    'option_id':option.id,
                    'option_title':option.title
                    })

            question_list.append({
                'question_id': q.id,
                'question_title': q.title,
                'score':q.score,
                # 'question_time':q.question_time,
                'option_list':option_list
                })

        quiz_list = [{
             'question_list':question_list
        }]


        return Response(quiz_list)


# Single Quiz View
class Question_View(DispatchLoginMixin):
    """
    Retrieve, update or delete a question instance.
    """
    def get_object(self, pk):
        try:
            return Question.objects.get(pk=pk)
        except Question.DoesNotExist:
            return Response('Question Not found')


    def get(self, request, pk):
       
        quiz_id = Quiz.objects.get(pk=pk)
        

        questions = Question.objects.filter(quiz_id=quiz_id.id)
        question_list = []
        for q in questions:
            option_list = []
            options = Option.objects.filter(question_id=q.id)
            for option in options:
                option_list.append({
                    'option_id':option.id,
                    'option_title':option.title,
                    'option_correct':option.is_correct,
                    })

            question_list.append({
                'question_id': q.id,
                'question_title': q.title,
                'score':q.score,
                'timer':q.timer,
                'correct':str(q.question_options.get(is_correct=True)),
                'explaination':q.explaination,
                # 'question_time':q.question_time,
                'option_list':option_list
                })

        # quiz_list = [{
        #      'quiz_name':quiz_id.title,
        #      'question_list':question_list
        # }]


        return Response(question_list)

    # def get(self, request, pk):
    #     try:
    #         question_id = Question.objects.get(pk=pk)
    #     except Question.DoesNotExist:
    #         return Response('Question Not found')

    #     option = Option.objects.filter(question_id=question_id.id)
    #     option_list = []
    #     for o in option:
    #         option_list.append({
    #             'option_id':o.id,
    #             'option_title':o.title,
    #             'is_correct':o.is_correct,
    #             'option_active':o.active
    #             })

    #     data = {
    #     'question_id':question_id.id,
    #     'question_title':question_id.title,
    #     'question_active':question_id.active,
    #     'option_list':option_list
    #     }

    #     print(data)
    #     return Response(data)

    # def get(self, request, pk):
    #     question = self.get_object(pk)
    #     serializer = QuestionSerializer(question)
    #     return Response(serializer.data)

    def put(self, request,pk):
        question = self.get_object(pk)
        serializer = QuestionSerializer(question, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        question = self.get_object(pk)
        question.delete()
        return Response('1 question deleted', status=status.HTTP_204_NO_CONTENT)



# Option Api View
class OptionView(DispatchLoginMixin):
    """
    Retrieve and Create Option
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))

        title = data_json['title']
        question = Question.objects.get(id=data_json['question_id'])
        is_correct = data_json['is_correct']
        active = data_json['active']

        Option.objects.create(title=title, question=question, is_correct=is_correct, active=active)

        return Response('1 option available created', status=status.HTTP_201_CREATED)

    def get(self,request,*args,**kwargs):
        
        option = Option.objects.all()
        print(option)
        serializer = OptionSerializer(option, many=True)
        return Response(serializer.data)

class Option_View(DispatchLoginMixin):
    """
    Retrieve, update or delete a option instance.
    """
    def get_object(self, pk):
        try:
            return Option.objects.get(pk=pk)
        except Option.DoesNotExist:
            raise Http404

        # new for json data
    def get(self, request, pk):
        
        question_id = Question.objects.get(pk=pk)
        option = Option.objects.filter(question_id=question_id.id)
        option_list = []
        for o in option:
            option_list.append({
                'option_id':o.id,
                'option_title':o.title,
                'is_correct':o.is_correct,
                'option_active':o.active
                })

        data = {
        'question_id':question_id.id,
        'question_title':question_id.title,
        'question_active':question_id.active,
        'option_list':option_list
        }

        print(data)
        print("Hii brijesh")
        return Response(data)



    # def get(self, request, pk):
    #     option = self.get_object(pk)
    #     serializer = OptionSerializer(option)
    #     return Response(serializer.data)

    def put(self, request,pk):
        option = self.get_object(pk)
        serializer = OptionSerializer(option, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, pk):
        option = self.get_object(pk)
        option.delete()
        return Response('1 option deleted', status=status.HTTP_204_NO_CONTENT)
from pprint import pprint

# Answer Api View
class AnswerView(DispatchLoginMixin):
    """
    Retrieve and Create Answer
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))
        try:
            Answer.objects.create(
                question = Question.objects.get(id=data_json['question_id']),
                option = Option.objects.get(id=data_json['option_id']),
                quiz = Quiz.objects.get(id=data_json['quiz_id']),
                user = User.objects.get(id=self.valid_user.id)
                )

            if  Option.objects.filter(id=data_json['option_id'], is_correct=True):
                return Response('Answer Is Correct', status=status.HTTP_201_CREATED)
            else:
                return Response('Answer Wrong', status=status.HTTP_201_CREATED)
        except:
            return Response("Answer Not saved")


    def get(self,request,*args,**kwargs):
        scores = Answer.objects.filter(user_id = self.valid_user.id)
        obtain_marks = 0
        total_score = 0
        for score in scores:
            total_score = total_score + score.question.score
            # print(score.question.score)
            # print(score.user.username)
            # print(score.option.is_correct, "\n====")
            if score.option.is_correct:
                obtain_marks = obtain_marks + score.question.score
        # print("================================")
        # print(obtain_marks)
        # print(total_score)

        print("{0:.2f} %".format(float(obtain_marks)*(100.0/float(total_score))))

        print("#############################################")

        al = Answer.objects.all()
        userList = []
        for a in al:
            userList.append(a.user.id)
                
        myUserList = list(dict.fromkeys(userList))
        for my in myUserList:
            qid = Answer.objects.filter(user_id = my)
            qlist = []
            for q in qid:
                qlist.append(q.quiz.id)
            myqlist = list(dict.fromkeys(qlist))
            print(myqlist)
            print(my,"--")
            for q_id in myqlist:
                ss = Answer.objects.filter(quiz_id = q_id, user_id = my)

                obtain_marks = 0
                total_score = 0
                print(ss)
                for s in ss:
                    total_score = total_score + s.question.score
                    print("--------------------------------------------------------")
                    print(s.question.title)
                    print(s.question.id)
                    print(s.question.score)
                    print(s.quiz.id)
                    print(s.option.is_correct)
                
                    if s.option.is_correct:
                        obtain_marks = obtain_marks + s.question.score
                
                print("{0:.2f} %".format(float(obtain_marks)*(100.0/float(total_score))))

                print("===================================================================")
        
        

        print(myqlist)
        print(myUserList, "-----myUserList----- " )
        print("#############################################")
        

        answers = Answer.objects.all()
        answer_list = []
        for answer in answers:
            answer_list.append({
                "question":answer.question.title,
                "option":answer.option.title,
                "quiz":answer.quiz.title,
                "user":answer.user.username,
                "wrong":answer.option.is_correct
            })
        
        return Response(answer_list)
        

    # def get(self,request,*args,**kwargs):
    #     answer = Answer.objects.all()
    #     serializer = AnswerSerializer(answer, many=True)
    #     return Response(serializer.data)


#Random Quiz Views

class RsandomQuizView(DispatchLoginMixin):

    def get(self,request,*args,**kwargs):

        qIds = Quiz.objects.all()
        quizIdList = []
        for qId in qIds:
            quizIdList.append(qId.id)
        print(quizIdList)
       
        quiz_id = Quiz.objects.get(pk= random.choice(quizIdList))
     
        print(quiz_id.id)
        print(quiz_id.title)
        

        questions = Question.objects.filter(quiz_id=quiz_id.id)
        question_list = []
        for q in questions:
            option_list = []
            options = Option.objects.filter(question_id=q.id)
            for option in options:
                option_list.append({
                    'option_id':option.id,
                    'option_title':option.title,
                    'option_correct':option.is_correct,
                    })

            question_list.append({
                'question_id': q.id,
                'question_title': q.title,
                'score':q.score,
                'timer':q.timer,
                'explaination':q.explaination,
                'option_list':option_list
                })

        quiz_list = [{
            'quiz_id':quiz_id.id,
            'quiz_title':quiz_id.title,
            'question_list':question_list
        }]

        return Response(quiz_list)

# Random Question And Option To make a quiz
class RandomQuiz(DispatchLoginMixin):
    def get(self, request, *args, **kwargs):

        questions = Question.objects.all()
        question_list = []
        for question in questions:
            question_list.append(question.id)
        print(question_list)
        random.shuffle(question_list)
        print(question_list[:10]) 
        qList = []
        for qid in question_list[:10]:
            
            ops = Option.objects.filter(question_id = qid)
            
            op_list = []
            for op in ops:
                op_list.append(op.id)
            
            random.shuffle(op_list)
            print(op_list)
            oList = []
            for oid in op_list:
                o = Option.objects.get(id = oid)
                oList.append({
                    "option_id":o.id,
                    "option_title":o.title,
                    "is_correct":o.is_correct
                })
            

            q = Question.objects.get(id=qid)
            qList.append({
                "question_id":q.id,
                "score":q.score,
                "question_title":q.title,
                "option_list":oList
            })
        return Response(qList)


class QuizResultView(DispatchLoginMixin):
    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body.decode("utf-8"))
        try:
            qResult = QuizResult.objects.get(user = self.valid_user)
            try:
                obtain_mark = data_json['obtain_mark']
                total_mark= data_json['total_mark']
                if qResult:
                    if qResult.obtainMark <= data_json['obtain_mark']:

                        qResult.obtainMark = obtain_mark
                        qResult.totalMark= total_mark
                        qResult.save()
                    return Response({
                        "message":"Quiz Result Updated",
                        "quiz_esult":[{
                            "quiz_result_id":qResult.id,
                            "username":qResult.user.email,
                            "name":qResult.user.first_name + " "+qResult.user.last_name,
                            "obtain_mark":qResult.obtainMark,
                            "total_mark":qResult.totalMark
                        }]
                        })
            except KeyError:
                return Response({"Message":"Some Data Are Missing"})

        except QuizResult.DoesNotExist:
            try:
                qResult = QuizResult.objects.create(
                    user = self.valid_user,
                    obtainMark = data_json['obtain_mark'],
                    totalMark= data_json['total_mark']
                )
                return Response({
                    "message":"Quiz Result Submitted Success",
                    "quizResult":[{
                        "quiz_result_id":qResult.id,
                        "username":qResult.user.email,
                        "name":qResult.user.first_name + " "+qResult.user.last_name,
                        "obtain_mark":qResult.obtainMark,
                        "total_mark":qResult.totalMark
                        }]
                    })
            except:
                return Response({
                    "message":"Quiz Result Not Submitted"
                    })
    
    def get(self, request, *args, **kwargs):
        quizResults = QuizResult.objects.all()
        quiz_Result = []
        for quizResult in quizResults:
            perMark = float(quizResult.obtainMark)*(100.0/float(quizResult.totalMark))
            quiz_Result.append({
                "id":quizResult.id,
                "username":quizResult.user.username,
                "name":quizResult.user.first_name + " "+quizResult.user.last_name,
                "obtain_mark":quizResult.obtainMark,
                "total_mark":quizResult.totalMark,
                "percentage_mark":perMark
            })
        
        return Response({
            "message":"Result Details of All Employee",
            "quiz_result":quiz_Result
        })


class QuizCertificate(DispatchLoginMixin):
    def get(self, request, *args, **kwargs):

        certificate = QuizResult.objects.get(user = self.valid_user)

        name = certificate.user.first_name + " "+ certificate.user.last_name

        print(name)
        perMark = float(certificate.obtainMark)*(100.0/float(certificate.totalMark))
        print(perMark)

        if perMark >=40.0:
            certificate_data ={
                "name":certificate.user.first_name + " "+ certificate.user.last_name,
                "percentage_mark":perMark,
                "certificate_issue_date":certificate.created.strftime("%Y-%m-%d")
            }
            return Response({
                "message":"Congratulations on your excellent success and good luck for more progress.",
                "certificate_data":certificate_data
                })
        else:
            # message = "You Are not Eligible for Certificate, please improve you quiz marks, You Got " +str(perMark)+" %"
            return Response({
                "message":"You Are not Eligible for Certificate, please improve you quiz marks, You Got " +str(perMark)+" %."
                })
        return Response("Certificate available for download")


            


# Answer Views
class Answer_View(DispatchLoginMixin):
    """
    Retrieve, update or delete a answer instance.
    """
    def get_object(self, pk):
        try:
            return Answer.objects.get(pk=pk)
        except Answer.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        answer = self.get_object(pk)
        serializer = AnswerSerializer(answer)
        return Response(serializer.data)

    # def put(self, request,pk):
    #     answer = self.get_object(pk)
    #     serializer = AnswerSerializer(answer, data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def delete(self, request, pk):
    #     answer = self.get_object(pk)
    #     answer.delete()
    #     return Response('1 answer deleted', status=status.HTTP_204_NO_CONTENT)

#Quiz Result Api
class Quiz_Result_View(DispatchLoginMixin):
    """
    Retrieve and Create Quiz_Result
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))
        # pprint(data_json)
        try:
            tms = Question.objects.filter(quiz_id = data_json['quiz_id'])
            m = 0
            for tm in tms:
                m +=tm.score

            print(m)
            Quiz_Result.objects.create(
                user=User.objects.get(id = self.valid_user.id),
                quiz=Quiz.objects.get(id = data_json['quiz_id']),
                obtain_marks=data_json['obtain_marks'],
                total_marks= m
                )

            return Response('Result created')
        except:
            return Response('Marks Does not update')


    def get(self,request,*args,**kwargs):

        result_list = []
        results = Quiz_Result.objects.all()
        quiz_result = []
        count1 = count2 = count3 = count4 = count5 = count6 = count7 = count8 = count9 = count10 = 0
        for result in results:
            
            perMark = float(result.obtain_marks)*(100.0/float(result.total_marks))
            if perMark >0 and perMark <=10:
                count1 = count1+1
            elif perMark >10 and perMark <=20:
                count2 =count2+1
            elif perMark >20 and perMark <=30:
                count3 = count3+1
            elif perMark >30 and perMark <=40:
                count4 =count4+1
            elif perMark >40 and perMark <=50:
                count5 = count5+1
            elif perMark >50 and perMark <=60:
                count6 =count6+1
            elif perMark >60 and perMark <=70:
                count7 = count7+1
            elif perMark >70 and perMark <=80:
                count8 =count8+1
            elif perMark >80 and perMark <=90:
                count9 = count9+1
            elif perMark >90 and perMark <=100:
                count10 = count10+1

            quiz_result.append({
                'result_id':result.id,
                'user':result.user.username,
                'quiz_title':result.quiz.title,
                'obtain_marks':result.obtain_marks,
                'total_marks':result.total_marks,
                "permark":str(perMark)+" %",
                'quiz_date':result.created.strftime("%Y-%m-%d %H:%M:%S")
                })

        quizResultCount = {
            "count1":count1,
            "count2":count2,
            "count3":count3, 
            "count4":count4,
            "count5":count5,
            "count6":count6,           
            "count7":count7,
            "count8":count8,
            "count9":count9,
            "count10":count10,
        }

        result_list.append({
            "quizResultCount":quizResultCount,
            "quiz_result":quiz_result
        })

        return Response(result_list)

class Quiz_Result_Views(DispatchLoginMixin):

    def get(self, request, pk):
        results = Quiz_Result.objects.filter(id=pk)
        quiz_result = []
        for result in results:
            perMark = float(result.obtain_marks)*(100.0/float(result.total_marks))
            quiz_result.append({
                'result_id':result.id,
                'user':result.user.username,
                'quiz_title':result.quiz.title,
                'obtain_marks':result.obtain_marks,
                'total_marks':result.total_marks,
                "permark":perMark,
                'date_of_issue':result.created.strftime("%Y-%m-%d %H:%M:%S")
                })

        return Response(quiz_result)


# Certificate View
from django.db.models import Max
class CertificateViews(DispatchLoginMixin):
    def get(self, request, *args, **kwargs):
        max_mark = Quiz_Result.objects.filter(user_id = self.valid_user.id).aggregate(Max('obtain_marks'))['obtain_marks__max']
        certificate = Quiz_Result.objects.get(obtain_marks = max_mark)

        name = certificate.user.first_name + " "+ certificate.user.last_name

        print(name)
        perMark = float(certificate.obtain_marks)*(100.0/float(certificate.total_marks))
        print(perMark)

        if perMark >=60.0:
            certificate_data ={
                "name":certificate.user.first_name + " "+ certificate.user.last_name,
                "percentage_mark":perMark,
                "certificate_issue_date":certificate.created.strftime("%Y-%m-%d")
            }
            return Response(certificate_data)
        else:
            return Response("You Are not Eligible for Certificate, please improve you quiz marks")
        return Response("Certificate available")

# Stories Api View
class StoriesView(DispatchLoginMixin):
    """
    Retrieve and Create Stories
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode('utf-8'))

        try:
            # harassment = data_json['harassment']
            harassment_type = int(data_json['harassment_type'])   
            city = data_json['city']
            incident = data_json['incident']
            reason = data_json['reason']
            action = data_json['action']
            # active = data_json['active']
            Stories.objects.create(harassment_type=harassment_type, city=city, incident=incident, reason=reason, action=action, active=True)
            return Response('Stories created', status=status.HTTP_201_CREATED)
        except:
            return Response('Some data Missing, Fill Before submit')

    def get(self,request,*args,**kwargs):

        stories = Stories.objects.all().order_by('-id')

        stories_list = []
        for story in stories:

            def create():
                time = datetime.now()

                if story.created.minute == time.minute:
                    return "Added " +str(time.second - story.created.second) + " second ago"

                elif story.created.hour == time.hour:
                    return "Added " + str(time.minute - story.created.minute) + " minute ago"

                elif story.created.day == time.day:
                    return "Added " +str(time.hour - story.created.hour) + " hours ago"

                elif story.created.month == time.month:
                    return "Added " + str(time.day - story.created.day) + " day ago"

                elif story.created.year == time.year:
                    return "Added " + str(time.month - story.created.month) + " month ago"

                else:
                    return "Added " + str(time.year - story.created.year) + " year ago"


            stories_list.append({
                'story_id':story.id,
                'harassment_type':story.get_harassment_type_display(),
                'city':story.city,
                'incident':story.incident,
                'reason':story.reason,
                'active':story.active,
                'publish_date':story.created.strftime("%d %b, %Y"),
                'created': create()
                })
        return Response(stories_list)

        # serializer = StoriesSerializer(stories, many=True)
        # return Response(serializer.data)

class Stories_View(DispatchLoginMixin):
    """
    Retrieve, update or delete a answer instance.
    """
    def get_object(self, pk):
        try:
            return Stories.objects.get(pk=pk)
        except Stories.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        try:
            story = Stories.objects.get(id=pk)
            stories_list =({
                'story_id':story.id,
                'harassment_type':story.get_harassment_type_display(),
                'city':story.city,
                'incident':story.incident,
                'reason':story.reason,
                'active':story.active,
                'publish_date':story.created.strftime("%d %b, %Y")
                })
            return Response(stories_list)

        except Stories.DoesNotExist:
            return Response("Story Not Found")

        

    def put(self, request,pk):
        stories = self.get_object(pk)
        serializer = StoriesSerializer(stories, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        stories = self.get_object(pk)
        stories.delete()
        return Response('stories deleted', status=status.HTTP_204_NO_CONTENT)


# Forced Learning Handel By Hr (Not for use at this time)

class ForcedLearningView(DispatchLoginMixin):
    """
    Change For Quize Type
    """
    def post(self,request,*args,**kwargs):
        data_json = json.loads(request.body.decode("utf-8"))
        try:
            work_profile = UserWorkProfile.objects.get(user=self.valid_user, user_type=2)
            if work_profile:
                try:
                    quiz = Quiz.objects.get(id=data_json['quiz_id'])

                    if quiz.forced_learning == True:
                        quiz.forced_learning = False
                    elif quiz.forced_learning == False:
                        quiz.forced_learning = True

                    quiz.save()
                    return Response('Quiz Updated For Forced Learning', status=status.HTTP_201_CREATED)
                except Quiz.DoesNotExist:
                    return Response('There Are Some missing data')
            else:
                return Response('{work_profile.user} is not in {work_profile.company}')

        except UserWorkProfile.DoesNotExist:
            return Response('User is not Hr of the company')
            