# Generated by Django 2.2 on 2019-10-14 11:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('learning', '0011_merge_20191014_1129'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='quiz',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='learning.Quiz'),
        ),
        migrations.AddField(
            model_name='answer',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='question',
            name='score',
            field=models.PositiveIntegerField(blank=True, default=0, null=True),
        ),
        migrations.AddField(
            model_name='question',
            name='timestamp',
            field=models.TimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='answer',
            name='option',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='learning.Option'),
        ),
        migrations.AlterField(
            model_name='stories',
            name='action',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='stories',
            name='harassment',
            field=models.CharField(blank=True, choices=[('H1', 'harassment1'), ('H2', 'harassment2'), ('H3', 'harassment3'), ('H4', 'harassment4')], max_length=1, null=True),
        ),
        migrations.AlterField(
            model_name='stories',
            name='reason',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
