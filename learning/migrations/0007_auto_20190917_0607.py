# Generated by Django 2.2 on 2019-09-17 06:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('learning', '0006_answer_chapter_option_question_quiz_section'),
    ]

    operations = [
        migrations.AlterField(
            model_name='option',
            name='question',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='question_options', to='learning.Question'),
        ),
        migrations.AlterField(
            model_name='question',
            name='quiz',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='quiz_questions', to='learning.Quiz'),
        ),
        migrations.AlterField(
            model_name='quiz',
            name='chapter',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='chapter_quizez', to='learning.Chapter'),
        ),
        migrations.AlterField(
            model_name='quiz',
            name='section',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='section_quizez', to='learning.Section'),
        ),
        migrations.AlterField(
            model_name='section',
            name='chapter',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='sections', to='learning.Chapter'),
        ),
    ]
