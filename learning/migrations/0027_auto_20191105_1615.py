# Generated by Django 2.2 on 2019-11-05 16:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('learning', '0026_chapter_language'),
    ]

    operations = [
        migrations.AddField(
            model_name='option',
            name='language',
            field=models.CharField(blank=True, choices=[('Hindi', 'HINDI'), ('English', 'ENGLISH'), ('Bengali', 'BENGALI'), ('Telugu', 'TELUGU'), ('Marathi', 'MARATHI'), ('Tamil', 'TAMIL'), ('Kannada', 'KANNADA'), ('Gujarati', 'GUJARATI'), ('Odia', 'ODIA')], default='English', help_text='Choose to serve content in other language for Company Default will be English', max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='question',
            name='language',
            field=models.CharField(blank=True, choices=[('Hindi', 'HINDI'), ('English', 'ENGLISH'), ('Bengali', 'BENGALI'), ('Telugu', 'TELUGU'), ('Marathi', 'MARATHI'), ('Tamil', 'TAMIL'), ('Kannada', 'KANNADA'), ('Gujarati', 'GUJARATI'), ('Odia', 'ODIA')], default='English', help_text='Choose to serve content in other language for Company Default will be English', max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='quiz',
            name='language',
            field=models.CharField(blank=True, choices=[('Hindi', 'HINDI'), ('English', 'ENGLISH'), ('Bengali', 'BENGALI'), ('Telugu', 'TELUGU'), ('Marathi', 'MARATHI'), ('Tamil', 'TAMIL'), ('Kannada', 'KANNADA'), ('Gujarati', 'GUJARATI'), ('Odia', 'ODIA')], default='English', help_text='Choose to serve content in other language for Company Default will be English', max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='section',
            name='language',
            field=models.CharField(blank=True, choices=[('Hindi', 'HINDI'), ('English', 'ENGLISH'), ('Bengali', 'BENGALI'), ('Telugu', 'TELUGU'), ('Marathi', 'MARATHI'), ('Tamil', 'TAMIL'), ('Kannada', 'KANNADA'), ('Gujarati', 'GUJARATI'), ('Odia', 'ODIA')], default='English', help_text='Choose to serve content in other language for Company Default will be English', max_length=20, null=True),
        ),
    ]
