
from django.db import models
from core.models import StatusMixin
from users.models import User
from datetime import datetime
from django.core.exceptions import ValidationError
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation

# Create your models here.

class Language(models.Model):
    LANGUAGE = (
        ('Hindi', 'HINDI'),
        ('English', 'ENGLISH'),
        ('Bengali', 'BENGALI'),
        ('Telugu', 'TELUGU'),
        ('Marathi', 'MARATHI'),
        ('Tamil', 'TAMIL'),
        ('Kannada', 'KANNADA'),
        ('Gujarati', 'GUJARATI'),
        ('Odia', 'ODIA'),
    )
    language = models.CharField(max_length=20, null=True, blank=True, 
                                choices=LANGUAGE, default='English',
                                help_text="Choose to serve content in other language for Company Default will be English")

    def __str__(self):
        return self.language

    def get_language_type(self):
        return Language[self.language]

    class Meta:
        abstract = True

class Chapter(StatusMixin,Language):
    title = models.CharField(max_length=400,null=True,blank=True)
    

    def __str__(self):
        return self.title

class Section(StatusMixin,Language):
    title = models.CharField(max_length=400,null=True,blank=True)
    chapter = models.ForeignKey(Chapter,null=True,blank=True,on_delete=models.CASCADE,related_name='sections')
    description = models.TextField(null=True,blank=True)

    def __str__(self):
        return self.title


class Quiz(StatusMixin,Language):
    title = models.CharField(max_length=500,null=True,blank= True)
    chapter = models.ForeignKey(Chapter,null=True,blank=True,on_delete=models.CASCADE,related_name='chapter_quizez')
    section = models.ForeignKey(Section,null=True,blank=True,on_delete=models.CASCADE,related_name='section_quizez')
    obtain_marks = models.IntegerField(null=True, blank=True)
    forced_learning = models.BooleanField(default=False) #HAndle buy Company HR
    totalMarks = models.PositiveIntegerField(null=True,blank=True,default=100)

    def save(self, *args, **kwargs):
        if self.title == None or self.title == "":
            self.title = "Quiz in section {}".format(self.section.title)
        super().save(*args, **kwargs)
    def __str__(self):
        return self.title


class Question(StatusMixin,Language):

    title = models.CharField(max_length=500,null=True,blank=True)
    quiz = models.ForeignKey(Quiz,null=True,blank=True,on_delete=models.CASCADE,related_name="quiz_questions")
    score = models.PositiveIntegerField(null=True,blank=True,default=10)
    timer = models.TimeField(null=True, blank=True)
    explaination = models.CharField(max_length=50, null=True, blank=True)
    # def validate_positive(value):
    #     if value<0:
    #         raise ValidationError(
    #             ('%(value)s is not a positive number'),params={'value': value},)

    # question_time = models.IntegerField(null=True,blank=True, validators=[validate_positive])

    def __str__(self):
        return self.title

class Option(StatusMixin,Language):
    title = models.CharField(max_length=300,blank=True,null=True)
    question = models.ForeignKey(Question,on_delete=models.CASCADE,null=True,blank=True,related_name="question_options")
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Answer(models.Model):
    question = models.ForeignKey(Question,null=False,blank=False,on_delete=models.CASCADE)
    option = models.ForeignKey(Option,null=True,blank=True,on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, null=True, blank=True, on_delete=models.CASCADE )
    
    def __str__(self):
        return self.user.username


class Quiz_Result(StatusMixin):
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, null=True, blank=True, on_delete=models.CASCADE)
    obtain_marks = models.IntegerField(null=True, blank=True)
    total_marks = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.user.username
        
class QuizResult(StatusMixin):
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    obtainMark = models.PositiveIntegerField(null=True, blank=True)
    totalMark = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.user.username


class Stories(StatusMixin):
    CHOICES = (
        ('H1', 'harassment1'),
        ('H2', 'harassment2'),
        ('H3', 'harassment3'),
        ('H4', 'harassment4'),
    )

    HARASSMENT_TYPE = (
        (1, 'Sexual harassment1'),
        (2, 'Sexual harassment2'),
        (3, 'Sexual harassment3'),
        (4, 'Sexual harassment4'),
        (5, 'Sexual harassment5')
        )

    harassment_type = models.IntegerField(blank=True, null=True, choices=HARASSMENT_TYPE)
    # harassment = models.CharField(blank=True, choices=CHOICES, max_length=2, null=True)
    city =  models.CharField(blank=True, max_length=50, null=True)
    incident = models.TextField(blank=True, null=True)
    reason = models.CharField(blank=True, max_length=200, null=True)
    action = models.CharField(blank=True, max_length=200, null=True)

    def get_harassement_type(self):
        return HARASSMENT_TYPE[self.harassment_type]

    

    def __str__(self):
        return self.reason
