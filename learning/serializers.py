from companies.mixins import RepresentationMixin
from .models import *


# class SectionSerializer(RepresentationMixin):
#     class Meta:
#         model = Section
#         fields = ['title','description','chapter']

# class ChapterSerializer(RepresentationMixin):
#     sections = SectionSerializer(many=True,read_only=True)
#     class Meta:
#         model = Chapter
#         fields = ['title','sections']

class ChapterSerializer(RepresentationMixin):
    class Meta:
        model = Chapter
        fields = ['id', 'title', 'created', 'updated', 'active']


class SectionSerializer(RepresentationMixin):
    class Meta:
        model = Section
        fields = ['id', 'title', 'chapter',
                  'description', 'created', 'updated', 'active']


class QuizSerializer(RepresentationMixin):
    class Meta:
        model = Quiz
        fields = ['id', 'title', 'chapter', 'obtain_marks' 'created',
                  'updated', 'section', 'active', 'forced_learning']


class QuestionSerializer(RepresentationMixin):
    class Meta:
        model = Question
        fields = ['id', 'title', 'quiz', 'score',
                  'created', 'updated', 'active']


class OptionSerializer(RepresentationMixin):
    class Meta:
        model = Option
        fields = ['id', 'title', 'question',
                  'is_correct', 'created', 'updated', 'active']


class AnswerSerializer(RepresentationMixin):
    class Meta:
        model = Answer
        fields = ['id', 'question', 'option', 'user', 'quiz']


class Quiz_ResultSerializer(RepresentationMixin):
    class Meta:
        model = Quiz_Result
        fields = "__all__"


class StoriesSerializer(RepresentationMixin):
    class Meta:
        model = Stories
        fields = "__all__"
