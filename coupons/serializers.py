from rest_framework import serializers
from coupons.models import Coupon

class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        # fields = "__all__"
        fields = ['id', 'code', 'valid_from', 'valid_to', 'discount', 'active']

