from django.contrib import admin
from easy_select2 import select2_modelform
from .models import *


#
# # Register your models here.
SelectForm = select2_modelform(User, attrs={'width': '500px'})
#
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = ['username', 'email', 'first_name', 'last_name', 'mobile', 'is_director']
    list_filter = ['username', ]
    search_fields = ('username', 'email' )
#
    form = SelectForm


# Sos Admin Model 
class SOSAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'relative_name', 'phone', 'email', 'relation', 'gender']

admin.site.register(SOS, SOSAdmin)

