from django.db import models
from django.contrib.auth.models import AbstractUser
from core.models import MobileMixin, StatusMixin


class User(AbstractUser, MobileMixin):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female')
    )
    is_director = models.BooleanField(default=False)
    gender = models.CharField(max_length=1, null=True,
                              blank=True, choices=GENDER_CHOICES)

    def get_gender(self):
        return GENDER_CHOICES[self.gender]


class SOS(StatusMixin):
    RELATION = (
        ('F', 'Family'),
        ('R', 'Friend'),
        ('O', 'Others'),
    )

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female')
    )

    user = models.ForeignKey(User, null=True, blank=True,
                             on_delete=models.CASCADE, related_name='sos_user')
    relative_name = models.CharField(max_length=200, null=True, blank=True)
    phone = models.CharField(max_length=13, null=True, blank=True)
    email = models.EmailField(
        max_length=200, null=True, blank=True, unique=True)
    relation = models.CharField(
        max_length=1, null=True, blank=True, choices=RELATION)
    gender = models.CharField(max_length=1, null=True,
                              blank=True, choices=GENDER_CHOICES)

    def get_relation(self):
        return RELATION[self.relation]

    def get_gender(self):
        return GENDER_CHOICES[self.gender]

    def __str__(self):
        return self.user.username+"'s " + "relative "+self.relative_name
