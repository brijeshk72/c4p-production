from django.shortcuts import render

# Create your views here.
from .models import User, SOS
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView
from .serializers import UserSerializer
from rest_framework import status
from companies.mixins import DispatchLoginMixin
import json


class UserCreate(CreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs): # <- here i forgot self
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        token, created = Token.objects.get_or_create(user=serializer.instance)
        return Response({'token': token.key}, status=status.HTTP_201_CREATED, headers=headers)


# SOS Model for family and friends 
class SOSView(DispatchLoginMixin):
    def post(self, request, *args, **kwargs):
        data_json = json.loads(request.body.decode("utf-8"))
        try:
            SOS.objects.create(
                user = self.valid_user,
                relative_name = data_json['relative_name'],
                phone = data_json['phone'],
                email = data_json['email'],
                relation = data_json['relation'],
                gender = data_json['gender']
            )
            return Response('Sos created')
        except KeyError:
            return Response('Some Data are Missing, Please Fill Again.')

    def get(self, request, *args, **kwargs):
        sos = SOS.objects.all().order_by('-id')
        sos_list = []
        for s in sos:
            sos_list.append({
                'sos_id':s.id,
                'user':s.user.username,
                'relative_name':s.relative_name,
                'phone':s.phone,
                'email':s.email,
                'relation':s.get_relation_display(),
                'gender':s.get_gender_display()
            })
        return Response(sos_list)

class SOSViews(DispatchLoginMixin):
    def get(self, request, pk):
        s = SOS.objects.get(pk=pk)
        sos_list = {
            'sos_id':s.id,
            'user':s.user.username,
            'relative_name':s.relative_name,
            'phone':s.phone,
            'email':s.email,
            'relation':s.get_relation_display(),
            'gender':s.get_gender_display()
        }
        return Response(sos_list)