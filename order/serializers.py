from rest_framework import serializers
from . models import *

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['company_name', 'paid', 'payumoney_id', 'coupon', 'discount', 'first_name',
         'last_name', 'mobile', 'email', 'gender', 'address', 'country', 'state', 'city', 'zip_code',
         'created', 'updated', 'active']


class OrderItemSerializer(serializers.ModelSerializer):
    order = OrderSerializer(read_only = True)
    class Meta:
        model = OrderItem
        fields = ['order', 'module', 'license', 'payable_amount', 'license_quantity']