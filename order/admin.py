from django.contrib import admin
from .models import Order, OrderItem
# Register your models here.


class OrderItemAdmin(admin.TabularInline):
	model = OrderItem
	raw_id_fields = ['module', 'license']
	extra = 1

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
	inlines = [OrderItemAdmin]
