from django.db import models
from core.models import *
from companies.models import *
# Create your models here.

class ComplaintDetail(models.Model):
    SEVERITY = (
        ('H', 'High'),
        ('M', 'Medium'),
        ('L', 'Low'),
        ('N', 'N/A'),
    )
    accussed_name = models.CharField(max_length=200,null=True,blank=True)
    accussed_department = models.CharField(max_length=200,null=True,blank=True)
    accussed_position = models.CharField(max_length=200,null=True,blank=True)
    repeat_times = models.PositiveIntegerField(null=True, blank=True, default=1) 
    severity = models.CharField(max_length=1,null=True,blank=True, choices=SEVERITY)
    description = models.TextField(null=True,blank=True)
    attachment = models.FileField(upload_to='complaint/attachments',null=True,blank=True)

    def get_severity(self):
        return SEVERITY[self.severity]
    
    def __str__(self):
        return self.accussed_name

class CompanyComplaints(StatusMixin):
    company_name = models.ForeignKey(Company,on_delete=models.CASCADE,null=True,blank=True,related_name='company_complaint')
    branch_name = models.ForeignKey(Branch,on_delete=models.DO_NOTHING,null=True,blank=True,related_name='branch_complaint')
    complaints_by = models.ForeignKey(UserWorkProfile,
                                     null=True,
                                     blank=True,
                                     on_delete=models.DO_NOTHING, related_name='complaint')

    complaint_details = models.OneToOneField(ComplaintDetail,null=True,blank=True,on_delete=models.DO_NOTHING,related_name='complaint_details')
    anonymous = models.BooleanField(default=False)
    is_resolved = models.BooleanField(default=False)

    def __str__(self):
        return self.company_name.name+" "+self.branch_name.name


class QueryMessage(StatusMixin):
    query = models.TextField(null=True, blank=True)
    attached_files = models.FileField(blank=True, null=True, upload_to ="QueryFiles")
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user)


# ###################### Unused model #########
class CompanyComplaint(StatusMixin):
    company = models.ForeignKey(Company,on_delete=models.CASCADE,null=True,blank=True,related_name='company_complaints')
    branch = models.ForeignKey(Branch,on_delete=models.DO_NOTHING,null=True,blank=True,related_name='branch_complaints')
    accepted_by = models.ForeignKey(UserWorkProfile,
                                    null=True,
                                    blank=True,
                                    on_delete=models.DO_NOTHING,related_name='accepted_by')

    complaint_by = models.ForeignKey(UserWorkProfile,
                                     null=True,
                                     blank=True,
                                     on_delete=models.DO_NOTHING, related_name='complaints')

    complaint_detail = models.OneToOneField(ComplaintDetail,null=True,blank=True,on_delete=models.DO_NOTHING,related_name='complaint_detail')
    is_resolved = models.BooleanField(default=False)
    is_anonymous = models.BooleanField(default=False)

    def __str__(self):
        return self.company.name+" "+self.branch.name

class ComplainReceive(StatusMixin):
    received_by = models.ForeignKey(UserWorkProfile, on_delete=models.CASCADE, null=True, blank=True)
    complain_record = models.ForeignKey(CompanyComplaints, on_delete=models.CASCADE, null=True, blank=True)
    is_resolved = models.BooleanField(default=False)

    def __str__(self):
        return self.received_by.user.username
#############################

