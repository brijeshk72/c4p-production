from django.db import models

# Create your models here.
# !! ProfileMixin Not To be Used for User Model !! #

class ProfileMixin(models.Model):

    GENDER_CHOICE = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    first_name = models.CharField(max_length=200,null=True,blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    mobile = models.CharField(max_length=15,null=True,blank=True)
    email = models.EmailField(max_length=250,null=True,blank=True)
    gender = models.CharField(choices=GENDER_CHOICE,null=True,blank=True,max_length=1)

    class Meta:
        abstract = True

class AddressMixin(models.Model):
    address = models.TextField(null=True,blank=True)
    country = models.CharField(max_length=200,null=True,blank=True)
    state = models.CharField(max_length=200,null=True,blank=True)
    city = models.CharField(max_length=200, null=True, blank=True)
    zip_code = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        abstract = True

class StatusMixin(models.Model):
    created = models.DateTimeField(null=True,blank=True,auto_now_add=True)
    updated = models.DateTimeField(null=True,blank=True,auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class MobileMixin(models.Model):
    mobile = models.CharField(null=True,blank=True,max_length=20)

    class Meta:
        abstract = True