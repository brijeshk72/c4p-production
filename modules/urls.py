from django.urls import path
from modules.serializers import ModuleSerializer
from . import views


app_name = 'modules'

urlpatterns = [
    path('api/module/', views.api_module_view, name='module'),
]