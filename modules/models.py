from django.db import models
from core.models import StatusMixin
# Create your models here.


class ModuleAndLicense(models.Model):
	name = models.CharField(max_length=200)
	slug = models.SlugField(max_length=200)
	description = models.TextField(blank=True)
	price = models.DecimalField(max_digits=10, decimal_places=2)

	class Meta:
		abstract = True


class Module(StatusMixin, ModuleAndLicense):
	pass


class License(StatusMixin, ModuleAndLicense):
	pass


# import random
# from django.conf import settings
# from django.db import IntegrityError
# from django.db import models
# from django.dispatch import Signal
# from django.utils import timezone
# from django.contrib.auth.models import User
# from core.utils import (
#     CODE_LENGTH,
#     CODE_CHARS,
#     SEGMENTED_CODES,
#     SEGMENT_LENGTH,
#     SEGMENT_SEPARATOR,
# )



# try:
#     from modules.feedbacks.models import Feedbacker
# except AttributeError:
#     from modules.feedbacks.models import Feedbacker
# redeem_done = Signal(providing_args=["coupon"])
#
# class CouponManager(models.Manager):
#     def create_coupon(self, type=None, user=None, valid_until=None, prefix="", campaign=None):
#         coupon = self.create(
#             code=LicenseCode.generate_code(prefix),
#             type=type,
#             user=user,
#             valid_until=valid_until,
#             campaign=campaign,
#         )
#         try:
#             coupon.save()
#         except IntegrityError:
#             # Try again with other code
#             return LicenseCode.objects.create_coupon(type, user, valid_until, prefix, campaign)
#         else:
#             return coupon
#
#     def create_coupons(self, type=None, user=None,  valid_until=None, prefix="", campaign=None):
#         coupons = []
#         coupons.append(self.create_coupon(type, user, valid_until, prefix, campaign))
#         return coupons
#
#     def used(self):
#         return self.exclude(redeemed_at=None)
#
#     def unused(self):
#         return self.filter(redeemed_at=None)
#
#     def expired(self):
#         return self.filter(valid_until__lt=timezone.now())
#
#
# @python_2_unicode_compatible
# class LicenseCode(models.Model):
#         code = models.CharField(("Code"), max_length=30, unique=True, blank=True,
#             help_text=("Leaving this field empty will generate a random code."))
#         type = models.CharField(("Type"), max_length=20, choices=COUPON_TYPES, null=True, blank=True)
#         user = models.ForeignKey(User, verbose_name=("Feedbacker"), related_name='coupon_code', null=True, blank=True,
#         help_text=("You may specify a Feedbacker you want to restrict this coupon to."))
#         created_at = models.DateTimeField(("Created at"), auto_now_add=True)
#         redeemed_at = models.DateTimeField(("Redeemed at"), blank=True, null=True)
#         valid_until = models.DateTimeField(("Valid until"), blank=True, null=True,
#             help_text=("Leave empty for coupons that never expire"))
#         campaign = models.ForeignKey('Campaign', verbose_name=("Campaign"), blank=True, null=True, related_name='coupons')
#
#         objects = CouponManager()
#
#         class Meta:
#             ordering = ['created_at']
#             verbose_name = ("Coupon Code")
#             verbose_name_plural = ("Coupon Codes")
#
#
#         def __str__(self):
#             return self.code
#
#         def save(self, *args, **kwargs):
#             if not self.code:
#                 self.code = LicenseCode.generate_code()
#             super(LicenseCode, self).save(*args, **kwargs)
#
#         @property
#         def is_active(self):
#             return  self.valid_until is not None and self.valid_until > timezone.now()
#
#         @property
#         def is_expired(self):
#             return self.valid_until is not None and self.valid_until < timezone.now()
#
#
#         @classmethod
#         def generate_code(cls, prefix="", segmented=SEGMENTED_CODES):
#             code = "".join(random.choice(CODE_CHARS) for i in range(CODE_LENGTH))
#             if segmented:
#                 code = SEGMENT_SEPARATOR.join([code[i:i+SEGMENT_LENGTH] for i in range(0, len(code), SEGMENT_LENGTH)])
#                 return code
#             else:
#                 return code
#
#
#         def redeem(self, user=None):
#             self.redeemed_at = timezone.now()
#             self.user = user
#             self.save()
#             redeem_done.send(sender=self.__class__, coupon=self)
#
#
# @python_2_unicode_compatible
# class Campaign(models.Model):
#     company = models.ForeignKey("companies.Store", verbose_name="Store", null=True, blank=True, related_name='campaign', on_delete=models.SET_NULL)
#     name = models.CharField(("Name"), max_length=255, unique=True)
#     description = models.TextField(("Description"), blank=True)
#     valid_upto = models.DateTimeField(blank=True, null=True)
#
#     class Meta:
#         ordering = ['name']
#         verbose_name = ("Campaign")
#         verbose_name_plural = ("Campaigns")
#
#     def __str__(self):
#         return self.name
