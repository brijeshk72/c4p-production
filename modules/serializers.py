from rest_framework import serializers
from . models import *

class ModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ['name', 'slug', 'description', 'price', 'updated', 'created', 'active']